const path = require('path');

module.exports = {

    entry: {
        bundle: './client/index.js'
    },

    output: {
        path: path.join(__dirname, './server/static/js'),
        filename: '[name].js'
    },

    resolve: {
        extensions: ['.js', '.jsx'],
        modules: ['node_modules']
    },

    resolveLoader: {
        modules: ['node_modules']
    },

    module: {
        rules: [
            {
                test: /(\.js$|\.jsx$)/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['react']
                    }
                }
            }
        ]
    }
};