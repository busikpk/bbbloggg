import {
    GET_COMMENTS_ERROR,
    GET_POST_ERROR,
    GET_POSTS_ERROR, LIKE_ACTION_ERROR,
    SEARCH_POSTS_ERROR
} from '../constans';

export function message (state = '', action) {
    switch (action.type) {
        case GET_POST_ERROR:
            return `can't get post;\nTry later;\nerror: ${action.payload}`;
        case GET_POSTS_ERROR:
            return `can't get posts;\nTry later;\nerror: ${action.payload}`;
        case SEARCH_POSTS_ERROR:
            return `can't find posts;\nTry later;\nerror: ${action.payload}`;
        case GET_COMMENTS_ERROR:
            return `can't get comments;\nTry later;\nerror: ${action.payload}`;
        case LIKE_ACTION_ERROR:
            return `can't get set/unset like;\nTry later;\nerror: ${action.payload}`;
        default:
            return state;
    }
}