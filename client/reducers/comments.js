import { GET_COMMENTS_SUCCESS } from '../constans';

const initialState = {
    objects: [],
    page: 1,
    hasNext: false,
    hasPrevious: false
};

export function comments(state = initialState, action) {
    switch (action.type) {
        case GET_COMMENTS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}