import {
    GET_POSTS_SUCCESS,
    POST_ADD_LIKE,
    POST_UNDO_LIKE,
    SEARCH_POSTS_SUCCESS
} from '../constans';

const initialState = {
    objects: [],
    page: 1,
    hasNext: false,
    hasPrevious: false,
    searched: false
};

export function posts(state = initialState, action) {
    switch (action.type) {
        case GET_POSTS_SUCCESS:
            return Object.assign({}, state, { searched: false }, action.payload);
        case POST_ADD_LIKE:
            state.objects.forEach(post => {
                if (post.id === action.payload.postId) {
                    post.likes.push({ user_id: action.payload.userId })
                }
            });
            return Object.assign({}, state);
        case POST_UNDO_LIKE:
            state.objects.forEach(post => {
                if (post.id === action.payload.postId) {
                    post.likes = post.likes.filter(({ user_id }) => user_id !== action.payload.userId);
                }
            });
            return Object.assign({}, state);
        case SEARCH_POSTS_SUCCESS:
            return Object.assign({}, state, { searched: true }, action.payload);
        default:
            return state;
    }

}