import {
    GET_POST_SUCCESS,
    POST_PAGE_ADD_LIKE,
    POST_PAGE_UNDO_LIKE
} from '../constans';

export function post(state = {}, action) {
    switch (action.type) {
        case GET_POST_SUCCESS:
            return action.payload;
        case POST_PAGE_ADD_LIKE:
            return Object.assign({}, state, { likes: [...state.likes, { user_id: action.payload.userId }]});
        case POST_PAGE_UNDO_LIKE:
            return Object.assign({}, state, { likes: state.likes.filter(({ user_id }) => user_id !== action.payload.userId)});
        default:
            return state;
    }
}