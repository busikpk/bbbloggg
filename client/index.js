import React from 'react';
import ReactDOM from 'react-dom';

import {createStore, combineReducers, applyMiddleware} from 'redux'
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import createHistory from 'history/createBrowserHistory'
import {Route, Router} from 'react-router'
import {ConnectedRouter, routerReducer, routerMiddleware, push} from 'react-router-redux'

import * as reducers from './reducers';

import Root from './containers/Root';
import PostCreator from './components/PostCreator';
import Post from './containers/Post';

const history = createHistory();

const store = createStore(
    combineReducers(Object.assign({}, reducers, { router: routerReducer })),
    applyMiddleware(routerMiddleware(history), thunk)
);


ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <div>
                <Route exact path="/" component={Root}/>
                <Route path="/posts/:id/" component={Post}/>
                <Route path="/create_post/" component={PostCreator}/>
            </div>
        </ConnectedRouter>
    </Provider>
    , document.getElementById('root'));
