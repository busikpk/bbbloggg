import { getCookie } from "./utils";

export const GET_POST_SUCCESS = 'GET_POST_SUCCESS';
export const GET_POST_ERROR = 'GET_POST_ERROR';

export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS';
export const GET_POSTS_ERROR = 'GET_POSTS_ERROR';

export const SEARCH_POSTS_SUCCESS = 'SEARCH_POST_SUCCESS';
export const SEARCH_POSTS_ERROR = 'SEARCH_POST_ERROR';

export const GET_COMMENTS_SUCCESS = 'GET_COMMENTS_SUCCESS';
export const GET_COMMENTS_ERROR = 'GET_COMMENT_ERROR';

export const POST_UNDO_LIKE = 'POST_UNDO_LIKE';
export const POST_ADD_LIKE = 'POST_ADD_LIKE';
export const POST_PAGE_UNDO_LIKE = 'POST_PAGE_UNDO_LIKE';
export const POST_PAGE_ADD_LIKE = 'POST_PAGE_ADD_LIKE';
export const LIKE_ACTION_ERROR ='POST_LIKE_ACTION_ERROR';

const HEADERS = new Headers();
HEADERS.append("Content-Type", "application/json");
HEADERS.append("X-CSRFToken", getCookie('csrftoken'));

export { HEADERS };
