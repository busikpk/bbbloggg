import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import * as actions from '../../actions';
import Pagination from '../../components/Pagination';
import PostInfo from '../../components/PostInfo';
import Header from '../../components/Header';
import SearchBar from '../../components/SearchBar';


class Root extends React.Component {

    constructor(props) {
        super(props);
        this.state = { searchText: '' };
        this.onPaginate = this.onPaginate.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    componentWillMount() {
        this.props.getPosts(1);
    }

    onPaginate(page) {
        if (this.props.posts.searched) {
            return this.props.searchPosts(this.state.searchText, page);
        }
        return this.props.getPosts(page);
    }

    onSearch(query) {
        this.setState({ searchText: query });
        this.props.searchPosts(query);
    }

    renderPosts() {
        return this.props.posts.objects.map(post => {
            return (
                <PostInfo
                    key={ `post_${post.id}` }
                    text={ `${post.body.substring(0, 100) } ...` }
                    onLike={ this.props.likePost } { ...post }
                />);
        });
    }

    renderSearchMessage() {
        return this.props.posts.searched ? (
            <div>
                <Link to={ "/" } onClick={ () => this.props.getPosts(1) }>
                    <h5>Go to all posts</h5>
                </Link>
                <h5>search results below</h5>
            </div>
        ) : null;
    }

    render() {
        return (
            <div>
                <Header onClick={ () => this.props.getPosts(1) } />
                <SearchBar onSearch={ this.onSearch }/>
                { this.renderSearchMessage() }
                <Link to="/create_post/">
                    <h3>Create Post</h3>
                </Link>
                { this.renderPosts() }
                <Pagination { ...this.props.posts } onPaginate={ this.onPaginate }/>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        posts: state.posts,
    }
}


function matchDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Root);