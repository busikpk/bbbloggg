import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Header from '../../components/Header'
import Pagination from '../../components/Pagination';
import { Comment, CommentCreateBlock } from '../../components/Comment';
import PostInfo from '../../components/PostInfo';
import * as actions from '../../actions';
import {HEADERS} from "../../constans";


class Post extends React.Component {

    constructor(props) {
        super(props);
        this.state = { commentText: ''};
        this.commentTextChange = this.commentTextChange.bind(this);
        this.commentCreate = this.commentCreate.bind(this);
    }

    componentWillMount() {
        const postId = this.props.match.params.id;
        this.props.getPost(postId);
        this.props.getComments(postId);
    }

    commentTextChange(event) {
        this.setState({ commentText: event.target.value });
    }

    commentCreate() {
        const postId = this.props.match.params.id;
        fetch(`/api/posts/${postId}/comments/`, {
            method: 'post',
            credentials: 'include',
            headers: HEADERS,
            body: JSON.stringify({ text: this.state.commentText})
        })
            .then(res => res.json())
            .then(res => {
                this.props.getComments(postId);
                this.setState({ commentText: '' });
            });
    }

    render() {
        if (this.props.post.id) {
            const { id, body, } = this.props.post;

            const onPaginate = (page) => this.props.getComments(id, page);
            const onLike = () => this.props.likePostOnPostPage(id);

            const comments = this.props.comments.objects.map(comment => {
                return <Comment key={ `comment_${comment.id}` } { ...comment }/>;
            });

            return (
                <div>
                    <Header onClick={ () => this.props.getPosts(1) } />
                    <PostInfo key={ `post_${id}` } text={ body } onLike={ onLike } { ...this.props.post }/>
                    <CommentCreateBlock
                        commentText={ this.state.commentText }
                        commentTextChange={ this.commentTextChange }
                        commentCreate={ this.commentCreate }
                    />
                    <div className="Comments">
                        <h3>Comments: </h3>
                        { comments }
                    </div>
                    <Pagination { ...this.props.comments } onPaginate={onPaginate}/>
                </div>
            );
        }
        return null;
    }
}

function mapStateToProps(state) {
    return {
        post: state.post,
        comments: state.comments
    }
}


function matchDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Post);