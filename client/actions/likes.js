import {
    HEADERS,
    POST_ADD_LIKE,
    POST_UNDO_LIKE,
    POST_PAGE_ADD_LIKE,
    POST_PAGE_UNDO_LIKE,
    LIKE_ACTION_ERROR
} from '../constans';
import { getCookie } from '../utils';


const like = (postId, okAction, errorAction) => dispatch => {
    fetch('/api/posts/like/', {
        method: 'post',
        headers: HEADERS,
        credentials: 'include',
        body: JSON.stringify({ post_id: postId })
    })
        .then(res => res.json())
        .then(result => {
            if (result.like === 'set') {
                dispatch(okAction(postId));
            } else {
                dispatch(errorAction(postId));
            }

        })
        .catch(err => dispatch(likeActionError(err)))
};

export function likePost(postId) {
    return like(postId, addLike, undoLike)
}

export function likePostOnPostPage(postId) {
    return like(postId, singlePostLike, singlePostUndoLike)
}

function singlePostLike(postId) {
    return {
        type: POST_PAGE_ADD_LIKE,
        payload: { postId, userId: +getCookie('userId') }
    }
}

function singlePostUndoLike(postId) {
    return {
        type: POST_PAGE_UNDO_LIKE,
        payload: { postId, userId: +getCookie('userId') }
    }
}

function undoLike(postId) {
    return {
        type: POST_UNDO_LIKE,
        payload: { postId, userId: +getCookie('userId') }
    }
}

function addLike(postId) {
    return {
        type: POST_ADD_LIKE,
        payload: { postId, userId: +getCookie('userId') }
    }
}

function likeActionError(payload) {
    return {
        type: LIKE_ACTION_ERROR,
        payload
    }
}