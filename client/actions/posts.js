import {
    GET_POSTS_SUCCESS,
    GET_POSTS_ERROR,
    GET_POST_SUCCESS,
    GET_POST_ERROR,
    SEARCH_POSTS_SUCCESS,
    SEARCH_POSTS_ERROR
} from '../constans';


export function getPosts(page) {
    //todo cache
    return (dispatch) => {
        fetch(`/api/posts/?page=${page}`, { method: 'get', credentials: 'include' })
            .then(res => res.json())
            .then(posts => dispatch(getPostsSuccess(posts)))
            .catch(err => dispatch(getPostsError(err)))
    }
}

export function getPost(postId) {
    //todo cache
    // maybe one store field for pages and posts ?
    return (dispatch) => {
        fetch(`/api/posts/${postId}/`, { method: 'get', credentials: 'include' })
            .then(res => res.json())
            .then(post => dispatch(getPostSuccess(post)))
            .catch(err => dispatch(getPostError(err)))
    }
}

export function searchPosts (query, page = 1) {
    return (dispatch) => {
        fetch(`/api/search/?query=${query}&page=${page}`, { method:'get', credentials: 'include' })
            .then(res => res.json())
            .then(res => dispatch(searchPostsSuccess(res)))
            .catch(err => dispatch(searchPostsError(err)))
    }
}

function searchPostsSuccess(payload) {
    return {
        type: SEARCH_POSTS_SUCCESS,
        payload
    }
}

function searchPostsError(payload) {
    return {
        type: SEARCH_POSTS_ERROR,
        payload
    }
}

function getPostsSuccess(payload) {
    return {
        type: GET_POSTS_SUCCESS,
        payload
    }
}

function getPostsError(payload) {
    return {
        type: GET_POSTS_ERROR,
        payload
    }
}

function getPostSuccess(payload) {
    return {
        type: GET_POST_SUCCESS,
        payload
    }
}

function getPostError(payload) {
    return {
        type: GET_POST_ERROR,
        payload
    }
}