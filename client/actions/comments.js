import {
    GET_COMMENTS_SUCCESS,
    GET_COMMENTS_ERROR
} from '../constans';

export function getComments(postId, page = 1) {
    return (dispatch) => {
        fetch(`/api/posts/${postId}/comments/?page=${page}`, { method: 'get', credentials: 'include' })
            .then(res => res.json())
            .then(res => dispatch(getCommentsSuccess(res)))
            .catch(err => dispatch(getCommentsError(err)))
    }
}


function getCommentsSuccess(payload) {
    return {
        type: GET_COMMENTS_SUCCESS,
        payload
    }
}

function getCommentsError(payload) {
    return {
        type: GET_COMMENTS_ERROR,
    }
}