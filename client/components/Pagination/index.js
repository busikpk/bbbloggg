import React from 'react';


const PageLink = ({ page, label, onPaginate }) => {
    const clickHandler = (event) => {
        event.preventDefault();
        onPaginate(page);
    };

    return <button className="PaginationButton" onClick={clickHandler}> {label} </button>;

};

const Pagination = ({ page, hasNext, hasPrevious, onPaginate}) => {
    const pageButtonPrev = hasPrevious ? (
        <PageLink
            page={ page - 1 }
            label='<'
            onPaginate={ onPaginate }
        />
    ) : null;

    const pageButtonNext = hasNext ? (
        <PageLink
            page={ page + 1 }
            label='>'
            onPaginate={ onPaginate }
        />
    ) : null;

    const pageIndicator = <span className='PaginationPageIndicator'> Page: { page } </span>;

    return (
        <div className='Pagination'>
            { pageButtonPrev }
            { pageIndicator }
            { pageButtonNext }
        </div>
    );
};


export default Pagination;