import React from 'react';
import { Link } from 'react-router-dom';

import Like from '../../components/Like';


const PostInfo = ({ id, title, text, dt, user, likes, onLike }) => {
    return (
        <div className="PostShort">
            <Link to={`/posts/${ id }/`}><h2>{ title }</h2></Link>
            <h5>By { user.first_name } { user.last_name } at { new Date(dt).toLocaleString() }</h5>
            <p>{ text }</p>
            <Like likes={ likes } onLike={ () => onLike(id) }/>
        </div>
    )
};

export default PostInfo;