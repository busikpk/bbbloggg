import React from 'react';

import { getCookie } from '../../utils';

const Like = ({ likes, onLike}) => {
    const isLiked = likes.map(({ user_id } ) => user_id).includes(+getCookie('userId'));
    const clickHandler = (event) => {
        event.preventDefault();
        onLike();
    };
    return (
        <div className="Like">
            <span>Likes: { likes.length }</span>
            <button
                className={`LikeButton ${isLiked ? 'Liked' : ''}`}
                onClick={clickHandler}
            >
                { isLiked ? '-' : '+' }
            </button>
        </div>
    )
};

export default Like;