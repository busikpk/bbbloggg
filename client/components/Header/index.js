import React from 'react';
import { Link } from 'react-router-dom';

const Header = ({ onClick }) => {
    return (
        <div className="Header">
            <Link to="/" onClick={ onClick }>
                <h1>Blog</h1>
            </Link>
        </div>
    )
};

export default Header;