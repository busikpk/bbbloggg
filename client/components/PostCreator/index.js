import React from 'react';
import { connect } from 'react-redux';
import Header from '../../components/Header';

import { HEADERS } from '../../constans';


export default class PostCreator extends React.Component {

    constructor(props) {
        super(props);
        this.state = { title: '', body: '' };
        this.titleChange = this.titleChange.bind(this);
        this.bodyChange = this.bodyChange.bind(this);
        this.onCreate = this.onCreate.bind(this);
    }

    titleChange(event) {
        this.setState({ title: event.target.value });
    }

    bodyChange(event) {
        this.setState({ body: event.target.value });
    }

    onCreate() {
        if (this.state.title && this.state.body) {
            fetch('/api/posts/', {
                method: 'post',
                headers: HEADERS,
                credentials: 'include',
                body: JSON.stringify({ title: this.state.title, body: this.state.body })
            })
                .then(res => res.json())
                .then(res => {
                    location.pathname = `/posts/${res.id}`;
                })
                .catch(err => console.error(err))
        }
    }


    render() {
        return (
            <div>
                <Header/>
                <div className="PostCreator">
                    <h3>Create New Post</h3>
                    <h4>Title</h4>
                    <input
                        value={this.state.title}
                        onChange={this.titleChange}
                        type="text"
                        name="title"
                        placeholder="Title"
                    />
                    <h4>Body</h4>
                    <textarea
                        value={this.state.body}
                        onChange={this.bodyChange}
                        name="body"
                        placeholder="Body"
                    />
                    <button onClick={this.onCreate} className="Create">Create</button>
                </div>
            </div>
        )
    }
}