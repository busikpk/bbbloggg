import React from 'react';


export default class SearchBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = { searchText: '' };
        this.onChange = this.onChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    onChange(event) {
        this.setState({ searchText: event.target.value });
    }

    onSearch() {
        if (this.state.searchText) {
            const query = this.state.searchText;
            this.setState({ searchText: '' }, () => this.props.onSearch(query));
        }
    }

    render() {
        return (
            <div className="SearchBar">
                <div className="SearchBarInner">
                    <input type="text" value={ this.state.searchText } onChange={this.onChange}/>
                    <button onClick={this.onSearch}>Search</button>
                </div>
            </div>
        );
    }
}