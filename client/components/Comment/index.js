import React from 'react';

const Comment = ({ id, user, dt, text }) => {
    return (
        <div className="Comment" key={id}>
            <h5>
                By { user.first_name } { user.last_name } at { new Date(dt).toLocaleString() }
            </h5>
            <pre>{ text }</pre>
        </div>
    );
};

const CommentCreateBlock = ({ commentText, commentTextChange, commentCreate}) => {
    return (
        <div className="CommentBlock">
            <h3>Add Comment</h3>
            <textarea value={commentText} onChange={commentTextChange}/>
            <button onClick={commentCreate}>Add</button>
        </div>
    );
};

export { Comment, CommentCreateBlock };