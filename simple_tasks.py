from functools import reduce
from pprint import pprint


def handle_numbers(number1, number2, number3):
    return reduce(lambda acc, item: acc if item % number3 else acc + 1, range(number1, number2 + 1), 0)


def handle_string(value):
    return reduce(
        lambda acc, item: (
            lambda numbers, letters: (
                numbers + item.isdigit(),
                letters + item.isalpha()
            ))(*acc),
        value, (0, 0))


def handle_list_of_tuples(list_):
    list_.sort(key=lambda item: (item[0], -int(item[1]), -int(item[2]), -int(item[3])))


if __name__ == '__main__':
    print(handle_numbers(1, 10, 4))

    print(handle_string("Hello world! 123!"))

    l = [
        ("Tom", "19", "167", "54"),
        ("Jony", "24", "180", "69"),
        ("Json", "21", "185", "75"),
        ("John", "27", "190", "87"),
        ("John", "25", "190", "87"),
        ("Jony", "24", "191", "98"),
        ("Jony", "24", "191", "97"),
    ]
    handle_list_of_tuples(l)
    pprint(l)
