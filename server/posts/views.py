import json

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db.utils import IntegrityError
from django.http.response import HttpResponseBadRequest, JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.postgres.search import SearchVector
from rest_framework import viewsets, response
from rest_framework.decorators import detail_route
from rest_framework import permissions

from .models import Post, Comment, Like
from .serializers import PostSerializer, CommentSerializer
from .utils import paginate_qset


@login_required(login_url='/sign_in')
def index(request, _=None):
    response_ = render(request, 'index.html')
    response_.set_cookie('userId', request.user.id)
    return response_


class PostsViewSet(viewsets.ViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request):
        page = paginate_qset(
            self.queryset.order_by('-dt'),
            PostSerializer,
            request.query_params.get('page', 1)
        )

        for post in page['objects']:
            post['liked'] = request.user.id in [like['user_id'] for like in post['likes']]

        return response.Response(page)

    def retrieve(self, request, pk=None):
        serializer = PostSerializer(self.queryset.get(pk=pk), many=False)
        return response.Response(serializer.data)

    def create(self, request):
        data = request.data
        try:
            post = Post(title=data['title'], body=data['body'], user_id=request.user.id)
            post.save()
        except IntegrityError:
            return HttpResponseBadRequest()

        return response.Response(PostSerializer(instance=post).data)

    @detail_route(methods=['GET', 'POST'])
    def comments(self, request, **kwargs):
        if request.method == 'POST':
            data = request.data
            try:
                comment = Comment(text=data['text'], user_id=request.user.id, post_id=kwargs['pk'])
                comment.save()
            except IntegrityError:
                return HttpResponseBadRequest()

            return response.Response(CommentSerializer(instance=comment).data)

        queryset = Comment.objects.all()
        comments = queryset.filter(post_id=kwargs['pk']).order_by('-dt')

        return response.Response(paginate_qset(
            comments,
            CommentSerializer,
            request.query_params.get('page', 1)
        ))


@login_required(login_url='/sign_in')
def post_like(request):
    post = json.loads(str(request.body, 'utf-8'))
    user = request.user
    post_id = post.get('post_id')

    if not post.get('post_id'):
        return HttpResponseBadRequest()

    try:
        like = Like.objects.get(user=user, post_id=post_id)
        like.delete()
        return JsonResponse({'like': 'unset'})
    except ObjectDoesNotExist:
        Like(user=user, post_id=post_id).save()
        return JsonResponse({'like': 'set'})


@login_required(login_url='/sign_id')
def search(request):
    if request.method == 'GET' and request.GET['query']:
        query = request.GET['query']
        page = request.GET.get('page', 1)
        return JsonResponse(paginate_qset(
            Post.objects.annotate(search=SearchVector('title', 'body')).filter(search=query).order_by('-dt'),
            PostSerializer,
            page
        ))
    return HttpResponseBadRequest()
