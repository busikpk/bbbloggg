from django.db import models

from server.authentication.models import User


class Post(models.Model):
    title = models.CharField(max_length=255, blank=False, null=False)
    body = models.CharField(max_length=4096, blank=False, null=False)
    dt = models.DateTimeField(null=False, auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Comment(models.Model):
    text = models.CharField(max_length=1024, blank=False, null=False)
    dt = models.DateTimeField(null=False, auto_now=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Like(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = (
            ("post", "user"),
        )
