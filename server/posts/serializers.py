from rest_framework import serializers

from .models import Post, Comment, User, Like


class _UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')


class _LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ('user_id',)


class PostSerializer(serializers.ModelSerializer):

    user = _UserSerializer(many=False, read_only=True)
    likes = serializers.SerializerMethodField('_get_likes')

    def _get_likes(self, post):
        return _LikeSerializer(Like.objects.filter(post_id=post.id), many=True).data

    class Meta:
        model = Post
        fields = ('id', 'title', 'body', 'dt', 'user', 'likes')


class CommentSerializer(serializers.ModelSerializer):

    post = serializers.PrimaryKeyRelatedField(many=False, read_only=True)
    user = _UserSerializer(many=False, read_only=True)

    class Meta:
        model = Comment
        fields = ('id', 'text', 'dt', 'user', 'post')
