from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from .views import index, PostsViewSet, post_like, search

router = DefaultRouter()
router.register(r'api/posts', PostsViewSet)

urlpatterns = [
    url(r'^$', index),
    url(r'^posts/([0-9]+)/$', index),
    url(r'^create_post/$', index),
    url(r'^api/search/$', search),
    url(r'^api/posts/like/$', post_like),
] + router.urls
