from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def paginate_qset(queryset, serializer, page, per_page=10):
    paginator = Paginator(queryset, per_page)

    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        page = 1
        objects = paginator.page(paginator.num_pages)

    return {
        'objects': serializer(instance=objects.object_list, many=True).data,
        'hasNext': objects.has_next(),
        'hasPrevious': objects.has_previous(),
        'page': int(page),
    }
