import logging

from django.conf import settings
from django.core.mail import send_mail

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)s %(message)s',
)


def hello(message):
    logging.debug(message.content)
    send_mail(
        'Blog account activation key',
        'Your activation key is: {}\n'
        'enter it in "blog host name where your registered"/approve_registration\n'
        'for account activation'.format(message.content['secret_key']),
        settings.EMAIL_HOST_USER,
        [message.content['email']],
        fail_silently=False
    )
