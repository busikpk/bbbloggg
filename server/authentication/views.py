import uuid

from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.views import View
from channels import Channel

from .forms import UserCreationForm, SecretKeyForm


_SIGN_UP_DTL = 'signup.html'
_SIGN_IN_DTL = 'signin.html'
_APPROVE_REGISTRATION_DTL = 'key.html'

_INDEX_URL = '/'
_SIGN_IN_URL = '/sign_in'
_APPROVE_REGISTRATION_URL = '/approve_registration'


class SignUpView(View):

    def get(self, request):
        form = UserCreationForm()
        return render(request, _SIGN_UP_DTL, {'form': form})

    def post(self, request):
        form = UserCreationForm(request.POST)

        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.secret_key = str(uuid.uuid4())
            user.username = user.email
            user.set_password(user.password)
            user.save()

            request.session['email'] = user.email
            request.session['secret_key'] = user.secret_key

            Channel('mail-channel').send({'email': user.email, 'secret_key': user.secret_key})

            return redirect(_APPROVE_REGISTRATION_URL)

        return render(request, _SIGN_UP_DTL, {'form': form})


class ApproveRegistration(View):

    def get(self, request):
        if not request.session.get('email'):
            return redirect(_INDEX_URL)

        form = SecretKeyForm()
        return render(request, _APPROVE_REGISTRATION_DTL, {'form': form})

    def post(self, request):
        username = request.session['email']
        password = request.session['secret_key']

        if not username:
            return redirect(_SIGN_IN_URL)

        form = SecretKeyForm(request.POST)

        if form.is_valid():
            user = authenticate(email=username, secret_key=password)
            if user and user.secret_key == form.cleaned_data.get('secret_key'):
                user.is_active = True
                user.save()
                login(request, user)
                return redirect(_INDEX_URL)
            else:
                form.add_error('secret_key', 'Invalid secret key')
                return render(request, _APPROVE_REGISTRATION_DTL, {'form': form})

        return redirect(_SIGN_IN_URL)
