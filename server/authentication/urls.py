from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import SignUpView, ApproveRegistration

urlpatterns = [
    url(r'^sign_in', auth_views.login, {'template_name': 'signin.html'}, name='login'),
    url(r'^sign_up', SignUpView.as_view()),
    url(r'^approve_registration', ApproveRegistration.as_view())
]
