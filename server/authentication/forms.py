from django.forms import (
    ModelForm,
    PasswordInput,
    CharField,
    DateField,
    DateInput,
    Form,
    TextInput
)
from .models import User


class CustomDateInput(DateInput):
    input_type = 'date'


class UserCreationForm(ModelForm):
    password = CharField(widget=PasswordInput())
    birthday = DateField(widget=CustomDateInput())

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'password',
            'birthday',
            'country',
            'city'
        )


class SecretKeyForm(Form):
    secret_key = CharField(widget=TextInput)
