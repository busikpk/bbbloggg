from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    email = models.EmailField(blank=False, null=False, unique=True)
    secret_key = models.CharField(max_length=64, blank=False, null=False)

    # custom fields
    birthday = models.DateField(null=False)
    country = models.CharField(max_length=128, blank=False, null=False)
    city = models.CharField(max_length=128, blank=False, null=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

