from .models import User
from django.core.exceptions import ObjectDoesNotExist


class UserAuthenticationByUsernameAndSecretKey(object):

    def authenticate(self, email, secret_key):
        try:
            return User.objects.get(email=email, secret_key=secret_key)
        except ObjectDoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None